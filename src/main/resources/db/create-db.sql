--DROP TABLE users IF EXISTS;

CREATE TABLE users (
  id       INTEGER IDENTITY PRIMARY KEY,
  login    VARCHAR(30),
  password VARCHAR(50),
  balance  INTEGER,
  role     VARCHAR(50),
  enabled  BOOLEAN
);