package com.gronerup.services;

import com.gronerup.User;

public interface IUserService {

    boolean isUserExists(String login);

    User getUserByLogin(String login);

    boolean saveUser(User user);

}
