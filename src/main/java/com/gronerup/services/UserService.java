package com.gronerup.services;

import com.gronerup.User;
import com.gronerup.repository.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService implements IUserService {

    @Autowired
    UserDAO userDAO;

    @Override
    public boolean isUserExists(String login) {
        return userDAO.isUserExists(login);
    }

    @Override
    public User getUserByLogin(String login) {
        return userDAO.getUserByLogin(login);
    }

    @Override
    public boolean saveUser(User user) {
        return userDAO.saveUser(user);
    }
}
