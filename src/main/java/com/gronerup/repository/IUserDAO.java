package com.gronerup.repository;

import com.gronerup.User;

public interface IUserDAO {

    boolean isUserExists(String login);

    User getUserByLogin(String login);

    boolean saveUser(User user);

}
