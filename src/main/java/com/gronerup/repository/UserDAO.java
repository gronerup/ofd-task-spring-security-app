package com.gronerup.repository;

import com.gronerup.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;


@Repository
public class UserDAO implements IUserDAO {

    @Autowired
    JdbcTemplate jdbcTemplate;


    public boolean isUserExists(String login) {

        String sql = "SELECT count(*) FROM users WHERE login = ?";
        boolean result = false;

        int count = jdbcTemplate.queryForObject(
                sql, new Object[]{login}, Integer.class);

        if (count > 0) {
            result = true;
        }

        return result;
    }


    public User getUserByLogin(String login) {
        String sql = "SELECT * FROM users WHERE login = ?";

        return jdbcTemplate.queryForObject(sql, new Object[]{login}, (rs, rowNum) ->
                new User(
                        rs.getString("login"),
                        rs.getString("password"),
                        rs.getInt("balance")

                ));

    }

    public boolean saveUser(User user) {

        if (isUserExists(user.getLogin())) {
            return false;
        }
        String sql = "INSERT INTO users(login, password,balance,enabled,role) VALUES(?,?,?,?,?)";
        jdbcTemplate.update(sql,
                user.getLogin(),
                "{noop}" + user.getPassword(),
                user.getBalance(),
                true,
                "ROLE_USER");
        return true;
    }

}

