package com.gronerup;


import com.gronerup.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

@org.springframework.stereotype.Controller
public class Controller {

    @Autowired
    UserService userService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView main(Principal principal) {
        User user = userService.getUserByLogin(principal.getName());
        return new ModelAndView("index").addObject("balance", user.getBalance());
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView showLogin() {
        return new ModelAndView("login");

    }

    @GetMapping("/login_error")
    public String login(@RequestParam(value = "login", required = false) String login,
                        @RequestParam(value = "password", required = false) String password,
                        HttpServletRequest request,
                        Model model) {
        try {
            String errorMessage = null;
            if (!userService.isUserExists(login)) {
                errorMessage = "Ошибка: 3-- Пользователя с таким логином не существует";
            } else if (!userService.getUserByLogin(login).getPassword().equals(password)) {
                errorMessage = "Ошибка: 4-- Неверный пароль";
            }

            model.addAttribute("error", errorMessage);
            return "login";
        } catch (Exception e) {
            model.addAttribute("error", "Ошибка: 2-- Техническая ошибка");
            return "login";
        }
    }


    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public ModelAndView registration() {
        ModelAndView modelAndView = new ModelAndView("registration");
        modelAndView.addObject("userForm", new User());

        return modelAndView;

    }

    @PostMapping("/registration")
    public String addUser(@ModelAttribute("userForm") User userForm, BindingResult bindingResult, Model model) {
        try {
            if (bindingResult.hasErrors()) {
                return "registration";
            }

            if (!userService.saveUser(userForm)) {
                model.addAttribute("error", "Ошибка: 1-- Пользователь с таким логином уже существует");
                return "registration";
            }

            return "redirect:/";
        } catch (Exception e) {
            model.addAttribute("error", "Ошибка: 2-- Техническая ошибка");
            return "registration";
        }
    }


}