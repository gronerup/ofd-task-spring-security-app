package com.gronerup;

public class User {

    private String login;
    private String password;
    private int balance = 0;


    public User(String login, String password, int balance) {
        this.login = login;
        this.password = password;
        this.balance = balance;
    }

    public User() {
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

}
