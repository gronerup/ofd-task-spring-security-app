<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>$Title$</title>
</head>
<body>
<H1>Ваш баланс</H1>
<h3>${balance}</h3>
<c:url var="logoutUrl" value="/j_spring_security_logout"/>
<form action="${logoutUrl}" id="logout" method="post">
    <input type="hidden" name="${_csrf.parameterName}"
           value="${_csrf.token}"/>
</form>
<a href="#" onclick="document.getElementById('logout').submit();">Выход</a>
</body>
</html>